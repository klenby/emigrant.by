<?php
	$settings = array(


		###### ADMIN ######
		'admin' => array(
			'max_upload_size' => '5242880',
		),
		########


		###### SYMPHONY ######
		'symphony' => array(
			'pagination_maximum_rows' => '99999',
			'lang' => 'en',
			'pages_table_nest_children' => 'no',
			'version' => '2.3',
			'cookie_prefix' => 'sym-',
			'session_gc_divisor' => '10',
		),
		########


		###### LOG ######
		'log' => array(
			'archive' => '1',
			'maxsize' => '102400',
		),
		########


		###### DATABASE ######
		'database' => array(
			'host' => 'localhost',
			'port' => '3306',
			'user' => 'root',
			'password' => null,
			'db' => 'emigrantby',
			'tbl_prefix' => 'sym_',
		),
		########


		###### PUBLIC ######
		'public' => array(
			'display_event_xml_in_source' => 'no',
		),
		########


		###### GENERAL ######
		'general' => array(
			'sitename' => 'Эмиграция в Чехию',
		),
		########


		###### FILE ######
		'file' => array(
			'write_mode' => '0644',
		),
		########


		###### DIRECTORY ######
		'directory' => array(
			'write_mode' => '0755',
		),
		########


		###### REGION ######
		'region' => array(
			'time_format' => 'H:i:s',
			'date_format' => 'j.n.Y',
			'datetime_separator' => ' ',
			'timezone' => 'Europe/Berlin',
		),
		########


		###### IMAGE ######
		'image' => array(
			'cache' => '1',
			'quality' => '90',
		),
		########


		###### MAINTENANCE_MODE ######
		'maintenance_mode' => array(
			'enabled' => 'no',
		),
		########


		###### SORTING ######
		'sorting' => array(
			'section_voprosy-i-otvety_sortby' => '3',
			'section_voprosy-i-otvety_order' => 'asc',
			'section_temy-voprosov-i-otvetov_sortby' => '28',
			'section_temy-voprosov-i-otvetov_order' => 'asc',
			'section_temy-o-chehii_sortby' => '29',
			'section_temy-o-chehii_order' => 'asc',
			'section_stati-o-chehii_sortby' => '30',
			'section_stati-o-chehii_order' => 'asc',
		),
		########
	);
