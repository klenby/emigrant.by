<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<!--		

		Главный шаблон указателя

		Параметры:
		nodelist — множество узлов, по которым будем строить указатель, должно совпадать с множеством узлов в ключе items-key, который должен быть создан в вызывающем шаблоне
		count-columns — кол-во колонок
		direction — направление заполнения колонок (0 — вертикальное, 1 — горизонтальное)

	-->

	<xsl:template name="make-indexes">			

		<xsl:param name="nodelist"/>
		<xsl:param name="key-name"/>
		<xsl:param name="count-columns" select="4"/>
		<xsl:param name="direction" select="0"/>
		<xsl:param name="key-symbol-position" select="1" />

		<!-- Для генерация списка блоков с буквами используем группировку Мюнха -->

		<xsl:variable name="blocks" select="$nodelist[generate-id(.) = generate-id(key($key-name, substring(., $key-symbol-position, 1)))]"/>		
				
		<div class="indexes">

				<xsl:choose>

					<!-- Вариант с горизонтальным заполнением -->

					<xsl:when test="$direction = 1">

						<xsl:for-each select="$blocks">				
							<xsl:sort select="."/>

							<!-- Проверяем, будет ли наш блок первым в строке, если да, то генерируем строку -->

							<xsl:if test="(position() - 1) mod $count-columns = 0">

								<xsl:apply-templates select="." mode="row-indexes">

									<xsl:with-param name="key-name" select="$key-name"/>
									<xsl:with-param name="blocks" select="$blocks"/>	
									<xsl:with-param name="start-position" select="position()"/>
									<xsl:with-param name="count-columns" select="$count-columns"/>
									<xsl:with-param name="key-symbol-position" select="$key-symbol-position" />

								</xsl:apply-templates>

							</xsl:if>

						</xsl:for-each>

					</xsl:when>

					<!-- Вариант с вертикальным заполнением -->

					<xsl:otherwise>

						<!-- Подсчитываем кол-во блоков в полностью заполненной колонке -->

						<xsl:variable name="blocks-per-column" select="ceiling(count($blocks) div $count-columns)"/>

						<!--

						Подсчитываем кол-во заполненнных и пустых колонок

							Пустые колонки могут возникнуть в случае, если кол-во колонок не делится без остатка
							на кол-во блоков в полностью заполненной колонке.

							Если пустые колонки не учитывать, то, например, при разбиении 12 блоков на 5 колонок будут
							заполнены только 4 колонки, так как при этом кол-во блоков в полностью заполненной колонке
							будет равно 3, соответственно все блоки блоки могут уместиться в 4 колонках, что 
							не удовлетворяет условию задачи.

						-->
						
						<xsl:variable name="count-filled-columns" select="ceiling(count($blocks) div $blocks-per-column)"/>
						<xsl:variable name="count-empty-columns" select="$count-columns - $count-filled-columns"/>

						<div class="row">

							<xsl:for-each select="$blocks">				
								<xsl:sort select="."/>

								<!-- Проверяем, будет ли наш блок первым в колонке -->

								<xsl:if test="(position() - 1) mod $blocks-per-column = 0">

									<!-- Высчитываем позицию текущей колонки -->

									<xsl:variable name="current-column" select="ceiling(position() div $blocks-per-column)"/>

									<!-- Уточняем кол-во блоков в этой колонке -->

									<xsl:variable name="blocks-per-this-column">							
										<xsl:choose>
											<xsl:when test="$current-column &gt; ($count-filled-columns - $count-empty-columns)">
												<xsl:value-of select="ceiling($blocks-per-column div 2)"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="$blocks-per-column"/>
											</xsl:otherwise>
										</xsl:choose>							
									</xsl:variable>

									<!-- Генерируем колонку -->

									<xsl:apply-templates select="." mode="column-indexes">

										<xsl:with-param name="key-name" select="$key-name"/>
										<xsl:with-param name="blocks" select="$blocks"/>
										<xsl:with-param name="blocks-per-this-column" select="number($blocks-per-this-column)"/>
										<xsl:with-param name="start-position" select="position()"/>
										<xsl:with-param name="key-symbol-position" select="$key-symbol-position" />

									</xsl:apply-templates>

									<!-- Проверяем, нужны ли дополнительные колонки, если нужны, то генерируем их -->

									<xsl:if test="$current-column &gt; $count-filled-columns - $count-empty-columns">

										<xsl:apply-templates select="." mode="column-indexes">

											<xsl:with-param name="key-name" select="$key-name"/>
											<xsl:with-param name="blocks" select="$blocks"/>
											<xsl:with-param name="blocks-per-this-column" select="$blocks-per-column - number($blocks-per-this-column)"/>
											<xsl:with-param name="start-position" select="position() + $blocks-per-this-column"/>
											<xsl:with-param name="key-symbol-position" select="$key-symbol-position" />

										</xsl:apply-templates>

									</xsl:if>

								</xsl:if>

							</xsl:for-each>

						</div>

					</xsl:otherwise>

				</xsl:choose>
	
		</div>

	</xsl:template>

	<!--

	Шаблон строки для варианта с горизонтальным заполнением

		Параметры:
		count-columns — кол-во колонок
		blocks — блоки с буквами
		start-position — позиция первого блока этой строки во множестве блоков

	-->

	<xsl:template match="* | @*" mode="row-indexes">

		<xsl:param name="key-name"/>
		<xsl:param name="count-columns"/>
		<xsl:param name="blocks"/>
		<xsl:param name="start-position"/>
		<xsl:param name="key-symbol-position" select="1" />

		<div class="row">

			<xsl:for-each select="$blocks">		
				<xsl:sort select="."/>

				<!-- Проверяем, является ли блок блоком этой строки, если да, то генерируем колонку и блок -->

				<xsl:if test="(position() &gt;= $start-position) and ((position() - $start-position) &lt; $count-columns)">

					<div class="span3">

						<xsl:apply-templates select="." mode="block-indexes">
							<xsl:with-param name="key-name" select="$key-name"/>
							<xsl:with-param name="key-symbol-position" select="$key-symbol-position" />
						</xsl:apply-templates>

					</div>

				</xsl:if>

			</xsl:for-each>

		</div>

	</xsl:template>

	<!--

	Шаблон колонки для варианта с вертикальным заполнением

		Параметры:	
		blocks — блоки с буквами
		blocks-per-this-column — кол-во блоков в колонке
		start-position — позиция первого блока этой колонки во множестве блоков

	-->

	<xsl:template match="* | @*" mode="column-indexes">

		<xsl:param name="key-name"/>
		<xsl:param name="blocks"/>
		<xsl:param name="blocks-per-this-column"/>
		<xsl:param name="start-position"/>
		<xsl:param name="key-symbol-position" select="1" />

		<xsl:variable name="end-position" select="$start-position + $blocks-per-this-column"/>

		<div class="span3">						

			<xsl:for-each select="$blocks">
				<xsl:sort select="."/>

				<!-- Проверяем, является ли блок блоком этой колонки, если да, то генерируем блок -->

				<xsl:if test="(position() &gt;= $start-position) and (position() &lt; $end-position)">

					<xsl:apply-templates select="." mode="block-indexes">
						<xsl:with-param name="key-name" select="$key-name"/>
						<xsl:with-param name="key-symbol-position" select="$key-symbol-position" />
					</xsl:apply-templates>

				</xsl:if>

			</xsl:for-each>

		</div>

	</xsl:template>

	<!-- Шаблон блока -->

	<xsl:template match="* | @*" mode="block-indexes">
	
		<xsl:param name="key-name"/>
		<xsl:param name="key-symbol-position" select="1" />

		<h3>
			<xsl:value-of select="substring(., $key-symbol-position, 1)"/>
		</h3>
		<ul>
			<xsl:apply-templates select="key($key-name, substring(., $key-symbol-position, 1))" mode="item-indexes">
				<xsl:sort select="."/>
			</xsl:apply-templates>
		</ul>

	</xsl:template>

	<!-- Шаблон для содержимого блока -->

	<xsl:template match="* | @*" mode="item-indexes">

		<li>
			<xsl:value-of select="."/>
		</li>

	</xsl:template>

</xsl:stylesheet>