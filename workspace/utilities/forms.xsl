<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template name="forms_faq">
	<xsl:param name="faq-section" select="/data/faq-sections/entry[1]/link/@handle" />
	<div id="form-result"></div>
		
	<form id="form-faq" method="post" action="/faq/ask/" enctype="multipart/form-data">
		<input name="MAX_FILE_SIZE" type="hidden" value="5242880" />
		
		<div class="control-group">
			<label>Ваша электронная почта:</label>
			<input class="span4 required email" name="fields[email]" type="email" placeholder="Электронная почта" />
		</div>
		
		<div class="control-group">
			<label>Тема вопроса:</label>
			<select class="span4" name="fields[section][]">
				<xsl:apply-templates select="/data/faq-sections/entry" mode="forms_faq_sections">
					<xsl:with-param name="faq-section" select="$faq-section" />
				</xsl:apply-templates>
			</select>
		</div>
		
		<div class="control-group">
			<label>Вопрос:</label>
			<textarea class="span4 required" rows="6" name="fields[question]" type="text" placeholder="Вопрос"></textarea>
		</div>
		
		<input class="btn submit disabled" name="action[form-question]" type="submit" value="Отправить" />
	</form>
</xsl:template>


<xsl:template match="entry" mode="forms_faq_sections">
	<xsl:param name="faq-section" select="/data/faq-sections/entry[1]/link/@handle" />
	<option value="{link/@handle}">
		<xsl:if test="link/@handle = $faq-section">
			<xsl:attribute name="selected">selected</xsl:attribute>
		</xsl:if>
		<xsl:value-of select="name/text()" />
	</option>
</xsl:template>


<!--
	FAQ по теме
-->
<xsl:template name="faq">
	<xsl:param name="faq-section" select="'programs'" />
	<p>
		<xsl:apply-templates select="/data/faq-questions/entry[section/item/@handle = $faq-section]" mode="faq" />
	</p>
	<p><i class="icon-circle-arrow-right"></i>&#160;<a href="/faq/section/{$faq-section}/">Все вопросы и ответы</a></p>
</xsl:template>

<xsl:template match="entry" mode="faq">
	<i class="icon-question-sign"></i>
	<xsl:text>&#160;</xsl:text>
	<a href="/faq/view/{link/@handle}/">
		<xsl:value-of select="question/node()" />
	</a>
	<br />
</xsl:template>


</xsl:stylesheet>