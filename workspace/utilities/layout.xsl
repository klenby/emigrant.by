<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:fl="http://symphony-cms.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:str="http://exslt.org/strings"
	extension-element-prefixes="exsl func str">

<xsl:import href="html.xsl" />
<xsl:import href="navigation.xsl" />
<xsl:import href="tools.xsl" />
<xsl:import href="forms.xsl" />

<xsl:output method="html" omit-xml-declaration="yes" indent="yes" />

<xsl:variable name="assets" select="concat( $workspace, '/assets' )" />
<xsl:variable name="css" select="concat( $assets, '/css' )" />
<xsl:variable name="js" select="concat( $assets, '/js' )" />
<xsl:variable name="img" select="concat( $assets, '/img' )" />

<xsl:variable name="nav" select="/data/global-navigation" />

<xsl:template match="/data">
	<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	<xsl:text>!DOCTYPE html</xsl:text>
	<xsl:text disable-output-escaping="yes">&gt;
</xsl:text>
	<html lang="ru">
		<xsl:call-template name="layout_head" />
		<xsl:call-template name="layout_body" />
	</html>
</xsl:template>

<!--
	HEAD Section
-->
<xsl:template name="layout_head">
	<head>
		<xsl:call-template name="layout_head_content-type" />
		<xsl:call-template name="layout_head_title" />
		<xsl:call-template name="layout_head_meta" />
		<xsl:call-template name="layout_head_description" />
		<xsl:call-template name="layout_head_icons" />
		<xsl:call-template name="layout_head_css" />
		<xsl:call-template name="layout_head_page_css" />
		<xsl:call-template name="layout_head_js" />
		<xsl:call-template name="layout_head_page_js" />
		<xsl:call-template name="layout_head_ga" />
	</head>
</xsl:template>

<xsl:template name="layout_head_content-type">
	<meta charset="utf-8" />
</xsl:template>

<xsl:template name="layout_head_title">
	<title>
		<xsl:value-of select="$page-title" />
		<xsl:text> — </xsl:text>
		<xsl:value-of select="$website-name" />
	</title>
</xsl:template>

<xsl:template name="layout_head_meta">
	<meta name="content-language" content="ru" />
	<meta name="revisit-after" content="7 days" />
	<meta name="robots" content="index, follow" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</xsl:template>

<xsl:template name="layout_head_description">
	<meta name="description" content="Эмиграция в Чехию — программы эмиграции, открытие шенгенской визы, вопросы и ответы по эмиграции, стоимоть эмиграции и дополнительных услуг. Фирма Talepet Group поможет вам в получении шенгенской визы." />
</xsl:template>

<xsl:template name="layout_head_icons">
	<!-- link rel="apple-touch-icon" href="{$root}/apple-touch-icon.png" / -->
	<link rel="shortcut icon" href="{$root}/favicon.ico" />
	<!-- link rel="alternate" type="application/rss+xml" title="" href="{$root}/rss/" / -->
</xsl:template>

<xsl:template name="layout_head_css">	
	<link href="{$css}/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="{$css}/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css' />
	
	<link href="{$css}/style.css" rel="stylesheet" />
</xsl:template>


<xsl:template name="layout_head_page_css" />


<xsl:template name="layout_head_js">
	<script>
		if (window != top) top.location.href = location.href;
	</script>
	
	<xsl:text disable-output-escaping="yes">&lt;</xsl:text>!--[if lt IE 9]<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
	<xsl:text disable-output-escaping="yes">&lt;</xsl:text>script src="<xsl:value-of select="$js" />js/html5.js"<xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>/script<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
	<xsl:text disable-output-escaping="yes">&lt;</xsl:text>![endif]--<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
</xsl:template>

<xsl:template name="layout_head_ga">
	<!-- script>
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'XX-XXXXXXXX-X']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script -->
</xsl:template>


<xsl:template name="layout_head_page_js" />
<!--
	/HEAD Section
-->

<!--
	BODY Section
-->
<xsl:template name="layout_body">
	<body>
		<xsl:call-template name="layout_body_header" />
		<xsl:call-template name="layout_body_content" />
		<xsl:call-template name="layout_body_footer" />
		<xsl:call-template name="layout_body_js" />
		<xsl:call-template name="layout_body_page_js" />
	</body>
</xsl:template>

<!-- Header -->
<xsl:template name="layout_body_header">
	<header>
		<div class="container">
			<xsl:call-template name="layout_body_header_content" />
		</div>
	</header>
</xsl:template>

<xsl:template name="layout_body_header_content">
	<div class="row">
		<div class="span12">
			<xsl:call-template name="navigation_logo" />
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<xsl:call-template name="navigation_menu" />
		</div>
	</div>
	<div class="row clearfix">
		<div class="span12">
			<hr />
		</div>
	</div>
</xsl:template>

<!--
	BODY
-->
<xsl:template name="layout_body_content">
	<div class="container">
		<div class="row">
			<div class="span3">
				<xsl:call-template name="navigation_menu" />
			</div>
			<div class="span9">
				<xsl:call-template name="layout_body_content_h1" />
				<xsl:call-template name="layout_body_content_content" />
			</div>
		</div>
	</div>
</xsl:template>

<xsl:template name="layout_body_content_h1">
	<h1>
		<xsl:value-of select="$page-title" />
	</h1>
</xsl:template>


<xsl:template name="layout_body_content_content" />


<xsl:template name="layout_body_footer">
	<footer>
		<div class="container">
			<xsl:call-template name="layout_body_footer_content" />
		</div>
	</footer>
</xsl:template>


<xsl:template name="layout_body_footer_content">
	<div class="row">
		<div class="span12">
			<hr />
			<p>2012, Talepet Group</p>
			<br /><br />
		</div>
	</div>
</xsl:template>

<xsl:template name="layout_body_js">
	<script src="{$js}/jquery-1.8.2.min.js"></script>
	<script src="{$js}/jquery.form.js"></script>
	<script src="{$js}/jquery.validate.js"></script>
	<script src="{$css}/bootstrap/js/bootstrap.min.js"></script>
	<script src="{$js}/script.js"></script>
</xsl:template>


<xsl:template name="layout_body_page_js" />
<!--
	/BODY Section
-->

</xsl:stylesheet>