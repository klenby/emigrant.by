<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template name="navigation_logo">
	<a href="/" id="logo">
		<img src="{$img}/logo.png" width="80" height="80" alt="{$website-name}" />
		<xsl:value-of select="$website-name" />
	</a>
</xsl:template>
	
	
<!--
	MAIN MENU
-->
<xsl:template name="navigation_menu">
	<div class="nav">
		<xsl:apply-templates select="/data/global-navigation" mode="navigation_menu" />
	</div>
</xsl:template>

<xsl:template match="global-navigation" mode="navigation_menu">
	<ul class="nav" role="nav">
		<xsl:apply-templates select="page[string( types/type ) != 'index']" />
	</ul>
</xsl:template>

<xsl:template match="page">
	<li>
		<a href="/{@handle}/">
			<xsl:value-of select="name/text()" />
		</a>
	</li>
</xsl:template>


<!--
	PROGRAMS MENU
-->
<xsl:template name="navigation_programs">
	<xsl:apply-templates select="/data/navigation-programs/page[@id != $current-page-id]" mode="navigation_programs" />
</xsl:template>

<xsl:template match="page" mode="navigation_programs">
	<xsl:variable name="page-handle" select="@handle" />
	<h4>
		<a href="/programs/{@handle}/">
			<xsl:value-of select="name/text()" />
		</a>
	</h4>
	<xsl:apply-templates select="/data/programs-page-content/entry[link/@handle = $page-handle]/description/node()" mode="html" />
</xsl:template>


<!--
	VISAS MENU
-->
<xsl:template name="navigation_visas">
	<xsl:apply-templates select="/data/navigation-visas/page[@id != $current-page-id]" mode="navigation_visas" />
</xsl:template>

<xsl:template match="page" mode="navigation_visas">
	<xsl:variable name="page-handle" select="@handle" />
	<h4>
		<a href="/visa/{@handle}/">
			<xsl:value-of select="name/text()" />
		</a>
	</h4>
	<xsl:apply-templates select="/data/visas-page-content/entry[link/@handle = $page-handle]/description/node()" mode="html" />
</xsl:template>


</xsl:stylesheet>