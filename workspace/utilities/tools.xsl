<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="tools_genitive-number.xsl" />
<xsl:import href="tools_format-date.xsl" />
<xsl:import href="tools_pagination.xsl" />
<xsl:import href="tools_alpha-indexes.xsl" />
<xsl:import href="tools_fl_utilities.xsl" />

</xsl:stylesheet>