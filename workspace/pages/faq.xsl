<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:fl="http://symphony-cms.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:str="http://exslt.org/strings"
	extension-element-prefixes="exsl func str">

<xsl:import href="../utilities/layout.xsl" />

<xsl:template name="layout_body_content">
	<div class="container">
		<xsl:call-template name="layout_body_content_content" />
	</div>
</xsl:template>


<xsl:template name="layout_head_description">
	<meta name="description" content="Ответы на все вопросы об эмиграции в Чехию и открытии собственной фирмы в Чехии." />
</xsl:template>


<xsl:template name="layout_body_content_content">
	<div class="row">
		<div class="span12">
			<p>
				<i class="icon-home"></i>
				<xsl:text> </xsl:text>
				<a href="/">Главная</a>
			</p>
			<h1>Вопросы и ответы</h1>
			<p>Ответы на вопросы от посетителей сайта</p>
			<xsl:apply-templates select="/data/faq-sections/entry" mode="faq_section" />
		</div>
	</div>
</xsl:template>


<xsl:template match="entry" mode="faq_section">
	<xsl:variable name="section" select="link/@handle"/>
	<h2>
		<a href="/faq/section/{link/@handle}/">
			<xsl:value-of select="name/text()" />
		</a>
		<xsl:text> </xsl:text>
		<small>
			<xsl:apply-templates select="content/node()" mode="html" />
		</small>
	</h2>
	
	<ul>
		<xsl:apply-templates select="/data/faq-questions/entry[section/item/@handle = $section]" mode="faq_question" />
	</ul>
	<p>
		<xsl:text>Все вопросы и ответы по теме «</xsl:text>
		<a href="/faq/section/{link/@handle}/">
			<xsl:value-of select="name/text()" />
		</a>
		<xsl:text>» (</xsl:text>
		<xsl:value-of select="@voprosy-i-otvety" />
		<xsl:text>)</xsl:text>
	</p>
</xsl:template>


<xsl:template match="entry" mode="faq_question">
	<li>
		<a href="/faq/view/{link/@handle}/">
			<xsl:value-of select="question/node()" />
		</a>
	</li>
</xsl:template>


</xsl:stylesheet>