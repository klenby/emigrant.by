<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:fl="http://symphony-cms.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:str="http://exslt.org/strings"
	extension-element-prefixes="exsl func str">

<xsl:import href="../utilities/layout.xsl" />

<xsl:template name="layout_body_content">
	<div class="container">
		<xsl:call-template name="layout_body_content_content" />
	</div>
</xsl:template>


<xsl:template name="layout_head_description">
	<meta name="description" content="Контактная информация, телефоны и адреса электронной почты для консультаций по эмиграции в Чехию и получению шенгенской визы" />
</xsl:template>


<xsl:template name="layout_body_content_content">
	<div class="row">
		<div class="span12">
			<h1>Контактная информация</h1>
			<div class="row">
				<div class="span4">
					<h3>в Чехии</h3>
					<p><strong><a href="http://www.justice.cz/xqw/xervlet/insl/index?sysinf.%40typ=or&amp;sysinf.%40strana=searchResults&amp;hledani.%40typ=subjekt&amp;hledani.podminka.subjekt=Talepet+Group&amp;hledani.podminka.ico=&amp;hledani.podminka.obec=&amp;hledani.podminka.spisZnacka.oddil=&amp;hledani.podminka.spisZnacka.vlozka=&amp;hledani.podminka.spisZnacka.soud=0&amp;hledani.format.pocet_polozek=50&amp;hledani.format.trideni=netridit&amp;hledani.format.typHledani=x*&amp;hledani.format.obchodniJmeno=platne&amp;hledani.podminka.pravniForma=-1&amp;hledani.podminka.ulice=">Talepet Group, S.R.O.</a></strong><br />
					Lidicka 534/75, 360 20 Karlovy Vary,<br />
					Drahovice, IC 27990745</p>
					<h4>Контактные телефоны в Чехии</h4>
					<p>+420 608-582-536<br />
					+420 607-391-948</p>
				</div>
				<div class="span4">
					<h3>в Беларуси</h3>
					<p>Услуги консультанта на русском языке:<br />
					+375 29 897-49-34<br />
					+375 33 317-18-35</p>
				</div>
				<div class="span4">
					<h3>Электронная почта</h3>
					<p><i class="icon-envelope"></i>&#160;<a href="mailto:info@emigrant.by">info@emigrant.by</a></p>
				</div>
			</div>
		</div>
	</div>
</xsl:template>

</xsl:stylesheet>