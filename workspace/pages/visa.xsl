<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:fl="http://symphony-cms.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:str="http://exslt.org/strings"
	extension-element-prefixes="exsl func str">

<xsl:import href="../utilities/layout.xsl" />

<xsl:template name="layout_body_content">
	<div class="container">
		<xsl:call-template name="layout_body_content_content" />
	</div>
</xsl:template>


<xsl:template name="layout_head_description">
	<meta name="description" content="Шенгенские визы, мультивизы, достоинства и недостатки различных видов виз." />
</xsl:template>


<xsl:template name="layout_body_content_content">
	<div class="row">
		<div class="span8">
			<p>
				<i class="icon-home"></i>
				<xsl:text> </xsl:text>
				<a href="/">Главная</a>
			</p>
			<h1>Визы в Чехию</h1>
			
			<div class="row">
				<div class="span5">
					<p>Вне зависимости от того, в каком посольстве вы <strong>открываете визу</strong>, в чешском, немецком, испанском, вы получаете <em>шенгенскую визу</em>. Однако стоит учесть, что в каждом посольстве свои правила и порядки. Поэтому лучше всего обратиться к специалистам, которые проконсультируют вас по всем <a href="/faq/section/visas/">вопросам</a> и помогут с оформлением визы.</p>
					
					<blockquote>
						<h3><a href="/visa/business-visa/">Бизнес-виза</a></h3>
						<p>Виза, выдаваемая предпринимателям для занятия бизнесом в Чехии</p>
						
						<h3><a href="/visa/work-visa/">Рабочая виза</a></h3>
						<p>Виза для наемных работников в Чешской фирме</p>
					</blockquote>
					
					
				</div>
				<div class="span3">
					<ul class="thumbnails">
						<li class="span3">
							<div class="thumbnail">
								<img alt="Страны Шенгенской зоны" src="{$img}/content/visa/schengen_agreement_map.png" />
								<div class="caption">
									<p><a href="/faq/view/kakie-strany-vhodyat-v-shengen/">Страны Шенгенской зоны</a><br /><small>(отмечены синим цветом)</small></p>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
				
			
			
			
			
			<div class="row">
				<div class="span4">
					<h2>Шенгенская виза</h2>
				
					<p><strong>Шенгенская виза</strong> — это виза, по которой вы сможете посещать любую <a href="/faq/view/kakie-strany-vhodyat-v-shengen/">страну Шенгенской зоны</a>, то есть практически все страны Европы.</p>
					
					<p>Шенгенские визы бывают <a href="/visa/tourist-visa/">туристическими</a> или <a href="/visa/work-visa/">рабочими</a>, однократными или многократными (мультивиза).</p>
				</div>
				<div class="span4">
					<blockquote>
						<h5><a href="/visa/student-visa/">Студенческая виза</a></h5>
						<p><small>Виза, выдаваемая студентам, обучающимся в Чешских учебных заведениях</small></p>
						
						<h5><a href="/visa/tourist-visa/">Туристическая виза</a></h5>
						<p><small>Виза выдается туристам для посещения Чехии</small></p>
					</blockquote>
				</div>
			</div>

			

			<p><strong>Однократная виза</strong> выдается на одно посещение страны и потом нужно открывать визу заново. <strong>Многократная виза</strong> или <em>мультивиза</em> дает право въезжать и выезжать из страны сколько угодно раз, но только в пределах срока действия вашей визы.</p>

			<p><strong>Рабочие визы</strong> выдаются для работы в определенной стране, откуда вас пригласили на работу, например, <a href="/visa/work-visa/">чешская рабочая виза</a>. Но все равно, это виза тоже шенгенская и дает вам право передвигаться по всем странам Шенгенского договора.</p>
			
			<p><em>Срок действия визы</em> зависит от вида самой визы. Например, туристическая виза выдается на срок от 7 дней, а <a href="/visa/business-visa/">бизнес-визу</a> можно открыть на срок до 1 года.</p>
			
			<p>Компания Talepet Group поможет вам с консультациями и открытием <a href="/visa/business-visa/"><strong>бизнес-визы</strong></a> или <a href="/visa/work-visa/"><strong>чешской рабочей визы</strong></a>.</p>
		</div>
		<div class="span4">
			<h2>Вопросы и ответы</h2>
			<p>По визам</p>
			<xsl:call-template name="faq">
				<xsl:with-param name="faq-section" select="'visas'" />
			</xsl:call-template>
			
			<h2>Задать вопрос</h2>
			<xsl:call-template name="forms_faq">
				<xsl:with-param name="faq-section" select="'visas'" />
			</xsl:call-template>
		</div>
	</div>
</xsl:template>

</xsl:stylesheet>