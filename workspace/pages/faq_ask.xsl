<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" omit-xml-declaration="yes" indent="yes" />

<xsl:template match="/">
	<xsl:comment>
		<xsl:copy-of select="/" />
	</xsl:comment>
	
	<response>
		<result>
			<xsl:value-of select="/data/events/form-question/@result" />
		</result>
		<message>
			<xsl:choose>
				<xsl:when test="/data/events/form-question/@result = 'success'">
					<xsl:text>Ваш вопрос получен</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>При отправке вопроса произошла ошибка.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</message>
	</response>
	
</xsl:template>


</xsl:stylesheet>