<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:fl="http://symphony-cms.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:str="http://exslt.org/strings"
	extension-element-prefixes="exsl func str">

<xsl:import href="../utilities/layout.xsl" />

<xsl:template name="layout_body_content">
	<div class="container">
		<xsl:call-template name="layout_body_content_content" />
	</div>
</xsl:template>


<xsl:template name="layout_head_description">
	<meta name="description" content="Статьи и полезная информация о Чехии." />
</xsl:template>


<xsl:template name="layout_body_content_content">
	<div class="row">
		<div class="span8">
			<p>
				<i class="icon-home"></i>
				<xsl:text> </xsl:text>
				<a href="/">Главная</a>
			</p>
			<h1>О Чехии</h1>
			<p>Статьи и полезная информация про Чехию</p>
			<xsl:apply-templates select="/data/czech-sections/entry" mode="czech_section" />
		</div>
	</div>
</xsl:template>


<xsl:template match="entry" mode="czech_section">
	<xsl:variable name="section" select="link/@handle"/>
	<h2>
		<a href="/czech/section/{link/@handle}/">
			<xsl:value-of select="name/text()" />
		</a>
		<xsl:text> </xsl:text>
		<sup>
			<small>
				<xsl:value-of select="@stati-o-chehii" />
			</small>
		</sup>
	</h2>
	
	<ul>
		<xsl:apply-templates select="/data/czech-articles/entry[section/item/@handle = $section]" mode="czech_article" />
	</ul>
</xsl:template>


<xsl:template match="entry" mode="czech_article">
	<li>
		<a href="/czech/view/{link/@handle}/">
			<xsl:value-of select="name/text()" />
		</a>
	</li>
</xsl:template>




</xsl:stylesheet>