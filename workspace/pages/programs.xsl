<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:fl="http://symphony-cms.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:str="http://exslt.org/strings"
	extension-element-prefixes="exsl func str">

<xsl:import href="../utilities/layout.xsl" />

<xsl:template name="layout_body_content">
	<div class="container">
		<xsl:call-template name="layout_body_content_content" />
	</div>
</xsl:template>


<xsl:template name="layout_head_description">
	<meta name="description" content="Программы эмиграции в Чехию" />
</xsl:template>


<xsl:template name="layout_body_content_content">
	<div class="row">
		<div class="span8">
			<p>
				<i class="icon-home"></i>
				<xsl:text> </xsl:text>
				<a href="/">Главная</a>
			</p>
			<h1>Программы эмиграции</h1>
			<p>Компания «Talepet Group» предлагает на ваш выбор три основных программы эмиграции.</p>
			<div class="row line">
				<div class="span8">
					<h3>Программа «<a href="/programs/emigrant/">Эмигрант 100%</a>»</h3>
					<p>Разработанная нами программа по гарантированной и беззаботной эмиграции в Чехию</p>
				</div>
			</div>
			<div class="row line">
				<div class="span8">
					<h3><a href="/programs/firm/">Открытие фирмы</a> в Чехии</h3>
					<p>Программа эмиграции в Чехию по бизнес-визе, помощь и консультации при открытии вашей собственной фирмы</p>
				</div>
			</div>
			<div class="row line">
				<div class="span8">
					<h3>Инструкция по <a href="/programs/self/">самостоятельной эмиграции</a></h3>
					<p>Для тех кто решил самостояьельно провести весь процесс эмиграции</p>
				</div>
			</div>
		</div>
		<div class="span4">
			<h2>Вопросы и ответы</h2>
			<p>по программам эмиграции</p>
			<xsl:call-template name="faq">
				<xsl:with-param name="faq-section" select="'programs'" />
			</xsl:call-template>
		
			<h2>Задать вопрос</h2>
			<xsl:call-template name="forms_faq">
				<xsl:with-param name="faq-section" select="'programs'" />
			</xsl:call-template>
		</div>
	</div>
</xsl:template>

</xsl:stylesheet>