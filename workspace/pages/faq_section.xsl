<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:fl="http://symphony-cms.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:str="http://exslt.org/strings"
	extension-element-prefixes="exsl func str">

<xsl:import href="../utilities/layout.xsl" />

<xsl:template name="layout_body_content">
	<div class="container">
		<xsl:call-template name="layout_body_content_content" />
	</div>
</xsl:template>


<xsl:template name="layout_head_description">
	<meta name="description" content="{ string( /data/faq-sections/entry[link/@handle = $q]/name/text() ) }" />
</xsl:template>


<xsl:template name="layout_head_title">
	<title>
		<xsl:value-of select="/data/faq-sections/entry[link/@handle = $q]/name/text()" />
		<xsl:text> — </xsl:text>
		<xsl:value-of select="$website-name" />
	</title>
</xsl:template>


<xsl:template name="layout_body_content_content">
	<div class="row">
		<div class="span8">
			<p>
				<i class="icon-home"></i>
				<xsl:text> </xsl:text>
				<a href="/">Главная</a>
				<xsl:text> → </xsl:text>
				<a href="/faq/">Вопросы и ответы</a>
			</p>
			<h1>
				<xsl:value-of select="/data/faq-sections/entry[link/@handle = $q]/name/text()" />
			</h1>
			<xsl:apply-templates select="/data/faq-sections/entry[link/@handle = $q]/content/node()" mode="html" />
			<ul>
				<xsl:apply-templates select="/data/faq-section/entry" mode="faq_question" />
			</ul>
		</div>
		<div class="span4">
			<h2>Другие разделы</h2>
			<xsl:apply-templates select="/data/faq-sections/entry[link/@handle != $q]" mode="faq_other_sections" />
		</div>
	</div>
</xsl:template>


<xsl:template match="entry" mode="faq_question">
	<li>
		<a href="/faq/view/{link/@handle}/">
			<xsl:choose>
				<xsl:when test="important = 'Yes'">
					<strong>
						<xsl:value-of select="question/node()" />
					</strong>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="question/node()" />
				</xsl:otherwise>
			</xsl:choose>
		</a>
	</li>
</xsl:template>


<xsl:template match="entry" mode="faq_other_sections">
	<h3>
		<a href="/faq/section/{link/@handle}/">
			<xsl:value-of select="name/text()" />
		</a>
	</h3>
	<xsl:apply-templates select="content/node()" mode="html" />
</xsl:template>

</xsl:stylesheet>