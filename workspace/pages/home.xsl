<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:fl="http://symphony-cms.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:str="http://exslt.org/strings"
	extension-element-prefixes="exsl func str">

<xsl:import href="../utilities/layout.xsl" />
<xsl:import href="czech.xsl" />

<xsl:template name="layout_body_content">
	<div class="container">
		<div class="row">
			<div class="span12">
				<xsl:call-template name="home_teaser" />
			</div>
		</div>
	</div>
	<div class="container">
		<xsl:call-template name="layout_body_content_content" />
	</div>
</xsl:template>


<xsl:template name="home_teaser">
	<div class="teaser">
		<div class="teaser-img">
			<!-- img src="{$img}/teaser.jpg" width="100%" alt="Гарантированная и беззаботная эмиграция в Чехию"/ -->
		</div>
		<h1>
			Программа «Эмигрант 100%»
		</h1>
		<p>Гарантированная и беззаботная эмиграция в Чехию</p>
		<p>
			<br />
			<br />
			<a class="btn btn-success" href="/programs/emigrant/">Подробности программы</a>
		</p>
		
	</div>
</xsl:template>


<xsl:template name="layout_body_content_content">
	<div class="row">
		<div class="span8">
			<h2>Программы эмиграции</h2>
			<xsl:call-template name="navigation_programs" />

			<h2>Визы в Чехию</h2>
			<xsl:call-template name="navigation_visas" />
			
			<h2>О Чехии</h2>
			<xsl:call-template name="home_czech" />
		</div>
		<div class="span4">
			<h2>Вопросы и ответы</h2>
			<xsl:call-template name="home_faq" />
			
			<h2>Задать вопрос</h2>
			<xsl:call-template name="forms_faq" />
		</div>
	</div>
</xsl:template>


<!-- 
	ARTICLES FOR HOME PAGE
-->
<xsl:template name="home_czech">
	<p>
		<xsl:apply-templates select="/data/home-czech/entry" mode="home_czech" />
	</p>
	<p><i class="icon-circle-arrow-right"></i>&#160;<a href="/czech/">Энциклопедия Чехии</a></p>
</xsl:template>

<xsl:template match="entry" mode="home_czech">
	<i class="icon-info-sign"></i>
	<xsl:text>&#160;</xsl:text>
	<a href="/czech/view/{link/@handle}/">
		<xsl:value-of select="name/text()" />
	</a>
	<br />
</xsl:template>


<!-- 
	FAQ FOR HOME PAGE
-->
<xsl:template name="home_faq">
	<p>
		<xsl:apply-templates select="/data/home-faq/entry" mode="home_faq" />
	</p>
	<p><i class="icon-circle-arrow-right"></i>&#160;<a href="/faq/">Все вопросы и ответы</a></p>
</xsl:template>

<xsl:template match="entry" mode="home_faq">
	<i class="icon-question-sign"></i>
	<xsl:text>&#160;</xsl:text>
	<a href="/faq/view/{link/@handle}/">
		<xsl:value-of select="question/node()" />
	</a>
	<br />
</xsl:template>


<!--
	FAQ FORM FOR HOME PAGE
-->
<xsl:template name="home_faq_form">
	<form method="post" action="/contact/message/" enctype="multipart/form-data">
		<label>Ваша электронная почта:</label>
		<input class="span4" type="email" name="email" placeholder="ваша@электронная.почта"/>
		<label>Ваш вопрос:</label>
		<textarea class="span4" placeholder="Ваш вопрос" rows="5"></textarea>
		<div class="action">
			<input class="btn" type="submit" value="Отправить" />
		</div>
	</form>
</xsl:template>

</xsl:stylesheet>