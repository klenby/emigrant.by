<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:fl="http://symphony-cms.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:str="http://exslt.org/strings"
	extension-element-prefixes="exsl func str">

<xsl:import href="../utilities/layout.xsl" />

<xsl:template name="layout_body_content">
	<div class="container">
		<xsl:call-template name="layout_body_content_content" />
	</div>
</xsl:template>


<xsl:template name="layout_head_description">
	<meta name="description" content="{string( /data/czech-sections/entry[link/@handle = $q]/content/node() )}" />
</xsl:template>


<xsl:template name="layout_head_title">
	<title>
		<xsl:value-of select="string( /data/czech-article/entry/description/node() )" />
		<xsl:text> — </xsl:text>
		<xsl:value-of select="$website-name" />
	</title>
</xsl:template>


<xsl:template name="layout_body_content_content">
	<xsl:variable name="section" select="/data/czech-article/entry/section/item/@handle" />
	<div class="row">
		<div class="span8">
			<p>
				<i class="icon-home"></i>
				<xsl:text> </xsl:text>
				<a href="/">Главная</a>
				<xsl:text> → </xsl:text>
				<a href="/czech/">О Чехии</a>
				<xsl:text> → </xsl:text>
				<a href="/czech/section/{/data/czech-sections/entry[link/@handle = $section]/link/@handle}/">
					<xsl:value-of select="/data/czech-sections/entry[link/@handle = $section]/name/text()" />
				</a>
			</p>
			<h1>
				<xsl:value-of select="/data/czech-article/entry/name/text()" />
			</h1>
			<xsl:apply-templates select="/data/czech-article/entry/content/node()" mode="html" />

		</div>
	</div>
</xsl:template>


</xsl:stylesheet>