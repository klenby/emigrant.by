<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exsl="http://exslt.org/common"
	xmlns:fl="http://symphony-cms.com/functions"
	xmlns:func="http://exslt.org/functions"
	xmlns:str="http://exslt.org/strings"
	extension-element-prefixes="exsl func str">

<xsl:import href="../utilities/layout.xsl" />

<xsl:template name="layout_body_content">
	<div class="container">
		<xsl:call-template name="layout_body_content_content" />
	</div>
</xsl:template>


<xsl:template name="layout_head_description">
	<meta name="description" content="Инструкция по самостоятельной регистрации фирмы в Чехии" />
</xsl:template>


<xsl:template name="layout_body_content_content">
	<div class="row">
		<div class="span8">
			<h1>Инструкция по самостоятельной эмиграции в Чехию</h1>
			
			<p>Рекомендуется почитать на <a href="http://www.czech.cz/ru/66806-obshestvo-s-ogranichennoy-otvetstvennostyu">официальном сайте</a>, краткое пояснение, что есть что.</p>
			
			<blockquote>Основная информация об <a href="/czech/view/sro/">Обществе с ограниченной ответственностью (s. r. o.)</a>
			</blockquote>

			<p>Для случая когда все учредители присутствуют лично, вам понадобятся следующие документы, которые вы можете подготовить дома:</p>
			<ol>
				<li><strong>Определиться с количеством соучредителей и их долей в s.r.o.</strong><br />Здесь каждый для себя определяется сам, сколько, какая доля и какие права. В уставе вы можете зафиксировать любые права и обязаности.</li>
				<li><strong>Определиться с «управляющим вкладом в уставный капитал с.р.о.».</strong><br />Лицо которое будет открывать счет в банке от имени всех соучредителей.</li>
				<li><strong>Определиться с названием s.r.o.</strong><br />На сайте <a href="http://www.justice.cz/xqw/xervlet/insl/index">торгового суда</a> вводите желаемое название в графе <em>Název subjektu</em> и проверяете, чтобы ваше название было уникальным; если оно неуникально, то торговый суд откажет в регистрации.</li>
				<li><strong>Определиться с юридическим адресом</strong><br />Можете за 5000 крон арендовать в частной квартире, можете обратится в специальные агества предлагающие такую услугу например хороший <a href="http://www.office-advisor.com/en/portfolio/czech-republic/prague/registered-offices.html?cp=all-districts&amp;order=street">поисковик</a> таких адресов.</li>
				<li><strong>Определиться с видами деятельности.</strong><br />Не лицензируемые виды деятельности идут подвидами у «свободного вида деятельности».</li>
				<li><strong>Финансы для внесение уставного капитала в размере 200 000 крон</strong><br />Вносятся на банковский счет, в последствии легко снимаются после регистрации или остаются на счете. Вся сумма делится пропорцианально на доли соучредителей.</li>
				<li><strong>Справка о несудимости с родины для каждого соучредителя.</strong><br />Не старше 3-х месяцев, получается на родине.</li>
				<li><strong>Паспорта и желательно одному человеку второй документ</strong> удостоверяющий личность, хотя может и не потребоваться.</li>
				<li><strong>Адрес прописки на чешском языке</strong><br />Можете попросить сделать этот перевод у переводчика, когда будете подавать документы. Но легко можно сделать то же самое человеку который хоть чуть–чуть владеет чешским, т. к. адреса нужны только для отправки вам писем.</li>
			</ol>

			<h2>Перед тем как приехать в Прагу</h2>

			<p>Для себя вы должны решить вопрос, кто поможет вам в общении с нотариусом, т. к. только у него требуется понимание, какие вы документы подписываете и что там написано. Есть 3 пути решения: знать чешский, чешскоговорящие друзья в Праге, помощь студента переводчика коих хватает, если чуть поискать. Больше нигде знание языка вам особо не понадобится.</p>
			<p>По приезду решаете с юр адресом и получаете полный комплект документов на этот адрес. Обычно это договор и выписка из реестра недвижимости о том, что это помещение принадлежит лицу с которым вы заключили договор. Все это вы можете сделать до приезда, а по приезду забрать документы и оплатить адрес.</p>
			<p>С самого утра идете к нотариусу и назначаете дату оформления документов, говорите ему:</p>
			<ol>
				<li>Чтобы подготовил полный пакет документов на регистрацию фирмы, кроме справки о несудимости.</li>
				<li>Если есть особые пожелания о уставе компании предупреждаете, если нет, то говорите, что бы делал стандартный устав.</li>
				<li>Также отдаете ему паспортные данные и сообщаете доли учредителей.</li>
				<li>Говорите ему виды деятельности, в моем случае это «свободный вид», подвиды по нему регистрируются позже без изменения в уставе. Если у вас лицензируемый вид деятельности, то необходим другой пакет документов, какой точно не подскажу.</li>
				<li>Сообщаете юр адрес фирмы</li>
			</ol>
			<p>Очень рекомендуется делать это с самого утра, что бы остался запас времени на подготовку остальных документов. Нотариус назначит день и время, когда вы должны вернутся на подписание документов, обычно через 2–5 дней.</p>
			<p>Далее направляетесь к переводчику для завереного перевода справок о несудимости с родины. Список судебных переводчиков можно посмотреть <a href="http://www.privet.cz/index.php?option=com_mtree&amp;task=listcats&amp;cat_id=6&amp;Itemid=88889073">здесь</a>.</p>
			<p>Далее нужно прокатиться за справкой о несудимости в Чехии до станции Pražského povstání (точный адрес Soudní 1, Praha 4). Выйдя из метро переходите большую под мостом и следуйте к большому муниципальному зданию, оно сразу бросается в глаза. Вход со двора в небольшую стеклянную пристройку с надписью на двери Rejstrik trestu. На входе берете бланк и заполняете его по образцу, там все просто. Далее по электронной очереди походите к окошку и улыбаясь произносите заклинание «Dobrý den, prosím» отдаете паспорт, заявление и 50 крон сбора. Получаете справку и все так же улыбаясь говорите «Dekuji, neshledanou».</p>
			<p>В день Х вы являетесь на запланированную встречу с нотариусом на подписание Учредительного документа s.r.o. и прочих сопутствующих документов. К этому времени нотариус уже все подготовил и все мероприятие займет от 15 до 30 минут. Туда вы должны явится со знанием языка или носителем который будет вашим переводчиком.</p>
			<p>Нотариусу вы передаете:</p>
			<ol>
				<li>Паспорта.</li>
				<li>Оригиналы документов, полученные в качестве подтверждения юридического адреса общества.</li>
			</ol>
			<p>У нотариуса вы должны подписать учредительский договор, содержащий:</p>
			<ul>
				<li>Юридические имя и адрес общества;</li>
				<li>Определение учредителей путем указания юридического имени или имени и юридического адреса юридического лица или имени и адреса проживания физического лица;</li>
				<li>Предметы предпринимательской деятельности;</li>
				<li>Размер Уставного капитала и размер вклада каждого учредителя в Уставный капитал, включая способ и срок оплаты вклада;</li>
				<li>Имена и адреса проживания первых еднателей общества и способ, которым они представляются от имени общества;</li>
				<li>Имена и адреса проживания членов наблюдательного совета (если он будет создан) (примечание: создание наблюдательного совета для s.r.o. не обязательно);</li>
				<li>Определение управляющего вкладами в Уставный капитал;</li>
			</ul>
			<p>И дополнительный пакет документов, который вы просили подготовить:</p>
			<ol>
				<li>Заявление еднателя с образцом подписи.</li>
				<li>Ходатайство в соответствующий суд для регистрации s.r.o. в Торговом реестре.</li>
			</ol>
			<p>Следует попросить сделать заверенную копию учредительского договора, т.к. один экземпляр вы отдаете в Торговый реестр, а второй должен остаться у вас.</p>
			<p>Вся процедура у нотариуса обойдется примерно в 6000 крон, но стоимость зависит от необходимого кол–ва копий документов, хотя сильно и не изменится.</p>
			
			<h2>Следующий шаг</h2>
			
			<p>Регистрация учрежденной фирмы в предпринимательском ведомстве с целью получения идентификационный номер организации IČO. Если сейчас среда и на ваших часах не 6 вечера то вам сказочно повезло. Отправляйтесь по этому адресу в Živnostenský úřad. На первом этаже находите информационное окно и написав заведомо на листочке название вашей компании спрашиваете, заведомо включив смекалку как это сделать без знания чешского, в какой кабинет обратится. Девушка по первой букве ваше названия определит кабинет и скажет куда вам идти. Заодно спросите, где можно отснять ксерокопию документов. На что она обрадуется и скажет что у нее можно. Снимите ксерокопию Учредительского договора (веревочки все тоже) и документов о юр адресе.</p>
			<p>Возле необходимого вам кабинета найдете образец заявления на регистрацию, если вы оказались смекалистым парнем, то это заявление вы попросили сделать нотариуса. Если нет, то придется потратить пол часика нервных импульсов, что бы разобраться и заполнить его. Хотя там ничего сложного нет, все по образцу и информации из ваших документов. Только не забудьте коды видов деятельности, которые вы собираетесь регистрировать. Их надо подготовить заранее и в виде порядковых номеров вписать в заявление.</p>
			<p>Далее с заявлением, оригиналом и копией учредительского договора, оригиналом и копией документов юр адреса и паспортом проходите в кабинет. Делайте глаза кота Шрека, произносите заклинание выученное вами в реестре судимости и протягивайте документы. Вам мило улыбнутся, посмотрят документы, оформят, скажут оплатить гербовый сбор. Не забывайте забрать оригинал учредительского договора.</p>
			<p>Спуститесь вниз возле окна информации есть кассы, где вам надо оплатить гербовый сбор 1000 крон.</p>
			<p>Если этот этап квеста выполнен вами удачно, то у вас будет от 2х до 5 дней свободного времени, можете пить, курить и развлекаться, не забывая о дне когда надо забрать документы из предпринимательского ведомства.</p>
			<p>А заодно о том, что бы найти место где можно отсканировать документы собрать их в виде PDF файла и записать на CD.</p>
			<ol>
				<li>Оригинал (stejnopis) нотариальной записи Учредительного договора. Назвать его notarsky_zapis_spolecenske_smlouvy.pdf</li>
				<li>Оригинал Заявления каждого учредителя с образцом подписи. Назвать их prohlaseni_jednatele_1.pdf 2,3,4 в зависимости от количества учредителей.</li>
			</ol>
			<p>Размер передаваемых PDF файлов ограничен, и не может превысить 150 кВ на 1 страницу. Т. к. они выкладываются в публичном доступе в сеть.</p>
			<p>Диск укладывается в конверт или прозрачный кейс и подписывается названием компании.</p>
			<p>Документы забираете в том же кабинете в котором и подавали, все так же произносите заклинание, отдаете паспорт квитанцию об оплате.</p>
			<p>Получив на руки документ о том, что произведена запись вашей компании в предпринимательском ведомстве, отправляйтесь с паспортом, заявлением управляющего вкладом, документом о регистрации в предпринимательском ведомстве и 200 000 кронами в выбранный вами банк. На расстояние пешей прогулки располагаются филиалы всех крупных банков, выбор за вами. Как правило в каждом банке есть русско или англоговорящий менеджер. В банке просите открыть счет для фирмы. Сотрудники там сами знают, что надо и проблем не возникнет. От банка вы получите документ о том, что банк принял у вас уставной капитал.</p>
			<h2>Самый ответственный этап</h2>
			<p>Подача документов на регистрацию в Торговый суд. У вас на руках должны быть следующие документы:</p>
			<ol>
				<li>Справка о несудимости с родины на каждого учредителя.</li>
				<li>Судебный перевод справки о несудимости.</li>
				<li>Справка о несудимости в Чехии на каждого учредителя.</li>
				<li>Документ от банка о том, что они приняли деньги для внесения в уставной капитал.</li>
				<li>Заявления в соответствующий суд для регистрации s.r.o. (этот заявление может оформить нотариус, рекомендую, что бы не вникать в подробности и не сделать ошибку) Должно быть заверено у нотариуса.</li>
				<li>Один оригинал (stejnopis) нотариальной записи Учредительного договора подписанного у нотариуса.</li>
				<li>Заявление каждого учредителя с образцом подписи, которое так же готовил нотариус.</li>
				<li>Заявлением управляющего вкладом в Уставный капитал общества, все так же получили его от нотариуса.</li>
				<li>Документы, подтверждающие юридический адрес. Оригиналы.</li>
				<li>Оригинал или заверенная копия выписки с предпринимательского реестра (živnostenský řestřik). Который вы получили в предыдущем шаге. В этом документе надо обратить внимание на номер под названием Kod ID RZP, этот номер вы должны переписать в соответствующую графу заявления подаваемого в суд для регистрации s.r.o.. Нотариус вас должен предупредить во время подписания документов об этом нюансе и показать эту графу.</li>
				<li>Заявление участников рассмотрения с подписями, это заявление подготовит нотариус.</li>
				<li>Предварительно записанный диск с PDF сканами документов, о котором писалось выше.</li>
				<li>6000 крон наличными для оплаты гербового сбора.</li>
				<li>Для упоротых — ходатайство об отказе от апелляции. Автор не несет ответственности за данный пункт и не считает его разумным, но все же. Подробнее, что это чуть ниже.</li>
			</ol>
			<p>Со всем пакетом документов следуете по адресу Городской суд в Праге (Městský soud v Praze), Slezská 9, 120 00 Praha 2 (вход в здание слевой стороны, а не в главный вход). Поднимаетесь на второй этаж в окошке покупаете марки на 6000 крон и наклеиваете их на заявление в соответствующее место. Получаете номерок электронной очереди Návrh na zápis do obchodního rejstříku. Дожидаетесь своей очереди и улыбаясь произнеся заклинание подаете документы. Не забываем вежливо благодарить и улыбаться). Документы об открытии фирмы вы получите на адрес своей прописки, туда же придет письмо с паролем от датовой скранки.</p>
			<p>Суд рассматривает документы в течении недели, если ответ положительный то через 15 дней после принятия решения судом данные о вашей фирме появится в интернете. Если нет, то можете подать апелляцию, благо я не знаю как это делать ибо не сталкивался, чего и вам желаю.</p>
			<p>Можно воспользоваться лайвхаком и при подачи документов в суд написать ходатайство об отказе от апелляции, тогда ваша фирма при успешном решении суда появится в интернете через пару дней, а может даже и в тот же день. Я воспользовался этим безумным способом и все прошло гладко, но если бы появилась ошибка в документах, то пришлось бы готовить весь пакет документов заново, что было бы дорого и печально.</p>
			<p>И так, если вы подали данное ходатайство и вам повезло, то через 5–7 дней ваша фирма появится в интернете. И вы сможете взять выпис и открыть счет в банке. Для этого вам потребуется найти любое отделение СzechPoint, туда с паспортом и следующим заклинанием «Dobrý den, prosím výpis» и протянуть бумажку с написанным названием вашей компании. Стоит эта услуга около 100 крон.</p>
			<p>Далее следуйте в банк в который вы вносили деньги, находите того операциониста с которым общались, отдаете выпис который только что получили, свой паспорт а в замен получает договор с банком и необходимые коды от клиент–банка. Все счет открыт, ваш уставной капитал на этом счете. С этого дня вы имеете права деятельности.</p>
			<p>P. S. Но для того, что бы у вас не было проблем с законом, надо не забывать в течении 30 дней зарегистрироваться в налоговой. Можете обратится к любому бухгалтеру, который поможет вам в этом. А можете и сами прокатится до финансового ведомства к которому вы закреплены.</p>

			<p><small>По материалам <a href="http://cz.leprosorium.ru/comments/1201642">Лепры</a> </small></p>
		</div>
		<div class="span4">
			<h2>Другие программы</h2>
			<xsl:call-template name="navigation_programs" />
		
			<h2>Вопросы и ответы</h2>
			<p>по самостоятельной эмиграции</p>
			<xsl:call-template name="faq">
				<xsl:with-param name="faq-section" select="'self'" />
			</xsl:call-template>
			
			<h2>Задать вопрос</h2>
			<p>по самостоятельной эмиграции</p>
			<xsl:call-template name="forms_faq">
				<xsl:with-param name="faq-section" select="'self'" />
			</xsl:call-template>
		</div>
	</div>
</xsl:template>

</xsl:stylesheet>