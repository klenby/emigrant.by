<?php

	require_once(TOOLKIT . '/class.datasource.php');

	Class datasourceglobal_navigation extends NavigationDatasource{

		public $dsParamROOTELEMENT = 'global-navigation';
		public $dsParamORDER = 'desc';
		public $dsParamREDIRECTONEMPTY = 'no';
		

		

		

		public function __construct($env=NULL, $process_params=true){
			parent::__construct($env, $process_params);
			$this->_dependencies = array();
		}

		public function about(){
			return array(
				'name' => 'Global: Navigation',
				'author' => array(
					'name' => 'B Y',
					'website' => 'http://emigrant.by.localhost',
					'email' => 'by@klen.by'),
				'version' => 'Symphony 2.3',
				'release-date' => '2012-10-16T07:35:10+00:00'
			);
		}

		public function getSource(){
			return 'navigation';
		}

		public function allowEditorToParse(){
			return true;
		}

	}
