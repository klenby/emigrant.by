<?php

	require_once(TOOLKIT . '/class.datasource.php');

	Class datasourcefaq_section extends SectionDatasource{

		public $dsParamROOTELEMENT = 'faq-section';
		public $dsParamORDER = 'asc';
		public $dsParamPAGINATERESULTS = 'no';
		public $dsParamLIMIT = '20';
		public $dsParamSTARTPAGE = '1';
		public $dsParamREDIRECTONEMPTY = 'no';
		public $dsParamSORT = 'system:id';
		public $dsParamASSOCIATEDENTRYCOUNTS = 'no';
		

		public $dsParamFILTERS = array(
				'9' => '{$q}',
				'3' => 'yes',
		);
		

		public $dsParamINCLUDEDELEMENTS = array(
				'question: formatted',
				'link',
				'answer: formatted',
				'section',
				'important'
		);
		

		public function __construct($env=NULL, $process_params=true){
			parent::__construct($env, $process_params);
			$this->_dependencies = array();
		}

		public function about(){
			return array(
				'name' => 'FAQ: Section',
				'author' => array(
					'name' => 'B Y',
					'website' => 'http://emigrant.by.localhost',
					'email' => 'by@klen.by'),
				'version' => 'Symphony 2.3',
				'release-date' => '2012-10-29T12:10:16+00:00'
			);
		}

		public function getSource(){
			return '1';
		}

		public function allowEditorToParse(){
			return true;
		}

	}
