<?php

	require_once(TOOLKIT . '/class.datasource.php');

	Class datasourcenavigation_visas extends NavigationDatasource{

		public $dsParamROOTELEMENT = 'navigation-visas';
		public $dsParamORDER = 'desc';
		public $dsParamREDIRECTONEMPTY = 'no';
		

		public $dsParamFILTERS = array(
				'parent' => '/visa',
				'type' => 'visas',
		);
		

		

		public function __construct($env=NULL, $process_params=true){
			parent::__construct($env, $process_params);
			$this->_dependencies = array();
		}

		public function about(){
			return array(
				'name' => 'Navigation: Visas',
				'author' => array(
					'name' => 'B Y',
					'website' => 'http://emigrant.by.localhost',
					'email' => 'by@klen.by'),
				'version' => 'Symphony 2.3',
				'release-date' => '2012-10-17T10:12:30+00:00'
			);
		}

		public function getSource(){
			return 'navigation';
		}

		public function allowEditorToParse(){
			return true;
		}

	}
