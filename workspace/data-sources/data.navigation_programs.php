<?php

	require_once(TOOLKIT . '/class.datasource.php');

	Class datasourcenavigation_programs extends NavigationDatasource{

		public $dsParamROOTELEMENT = 'navigation-programs';
		public $dsParamORDER = 'desc';
		public $dsParamREDIRECTONEMPTY = 'no';
		

		public $dsParamFILTERS = array(
				'type' => 'program',
				'parent' => '/programs',
		);
		

		

		public function __construct($env=NULL, $process_params=true){
			parent::__construct($env, $process_params);
			$this->_dependencies = array();
		}

		public function about(){
			return array(
				'name' => 'Navigation: Programs',
				'author' => array(
					'name' => 'B Y',
					'website' => 'http://emigrant.by.localhost',
					'email' => 'by@klen.by'),
				'version' => 'Symphony 2.3',
				'release-date' => '2012-10-17T09:41:32+00:00'
			);
		}

		public function getSource(){
			return 'navigation';
		}

		public function allowEditorToParse(){
			return true;
		}

	}
