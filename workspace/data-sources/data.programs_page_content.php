<?php

	require_once(TOOLKIT . '/class.datasource.php');

	Class datasourceprograms_page_content extends SectionDatasource{

		public $dsParamROOTELEMENT = 'programs-page-content';
		public $dsParamORDER = 'desc';
		public $dsParamPAGINATERESULTS = 'no';
		public $dsParamLIMIT = '20';
		public $dsParamSTARTPAGE = '1';
		public $dsParamREDIRECTONEMPTY = 'no';
		public $dsParamSORT = 'system:id';
		public $dsParamASSOCIATEDENTRYCOUNTS = 'no';
		

		

		public $dsParamINCLUDEDELEMENTS = array(
				'name',
				'link',
				'description: formatted',
				'content: formatted'
		);
		

		public function __construct($env=NULL, $process_params=true){
			parent::__construct($env, $process_params);
			$this->_dependencies = array();
		}

		public function about(){
			return array(
				'name' => 'Programs: Page Content',
				'author' => array(
					'name' => 'B Y',
					'website' => 'http://emigrant.by.localhost',
					'email' => 'by@klen.by'),
				'version' => 'Symphony 2.3',
				'release-date' => '2012-10-17T10:09:23+00:00'
			);
		}

		public function getSource(){
			return '5';
		}

		public function allowEditorToParse(){
			return true;
		}

	}
