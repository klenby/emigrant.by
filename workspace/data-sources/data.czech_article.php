<?php

	require_once(TOOLKIT . '/class.datasource.php');

	Class datasourceczech_article extends SectionDatasource{

		public $dsParamROOTELEMENT = 'czech-article';
		public $dsParamORDER = 'desc';
		public $dsParamPAGINATERESULTS = 'yes';
		public $dsParamLIMIT = '1';
		public $dsParamSTARTPAGE = '1';
		public $dsParamREDIRECTONEMPTY = 'no';
		public $dsParamSORT = 'system:id';
		public $dsParamASSOCIATEDENTRYCOUNTS = 'no';
		

		public $dsParamFILTERS = array(
				'14' => '{$q}',
				'26' => 'yes',
		);
		

		public $dsParamINCLUDEDELEMENTS = array(
				'name',
				'link',
				'section',
				'description: formatted',
				'content: formatted'
		);
		

		public function __construct($env=NULL, $process_params=true){
			parent::__construct($env, $process_params);
			$this->_dependencies = array();
		}

		public function about(){
			return array(
				'name' => 'Czech: Article',
				'author' => array(
					'name' => 'B Y',
					'website' => 'http://emigrant.by.localhost',
					'email' => 'by@klen.by'),
				'version' => 'Symphony 2.3',
				'release-date' => '2012-11-02T11:46:21+00:00'
			);
		}

		public function getSource(){
			return '4';
		}

		public function allowEditorToParse(){
			return true;
		}

	}
