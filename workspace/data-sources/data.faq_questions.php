<?php

	require_once(TOOLKIT . '/class.datasource.php');

	Class datasourcefaq_questions extends SectionDatasource{

		public $dsParamROOTELEMENT = 'faq-questions';
		public $dsParamORDER = 'asc';
		public $dsParamPAGINATERESULTS = 'no';
		public $dsParamLIMIT = '20';
		public $dsParamSTARTPAGE = '1';
		public $dsParamREDIRECTONEMPTY = 'no';
		public $dsParamSORT = 'system:id';
		public $dsParamASSOCIATEDENTRYCOUNTS = 'no';
		

		public $dsParamFILTERS = array(
				'3' => 'yes',
				'4' => 'yes',
		);
		

		public $dsParamINCLUDEDELEMENTS = array(
				'question: formatted',
				'link',
				'answer: formatted',
				'section'
		);
		

		public function __construct($env=NULL, $process_params=true){
			parent::__construct($env, $process_params);
			$this->_dependencies = array();
		}

		public function about(){
			return array(
				'name' => 'FAQ: Questions',
				'author' => array(
					'name' => 'B Y',
					'website' => 'http://emigrant.by.localhost',
					'email' => 'by@klen.by'),
				'version' => 'Symphony 2.3',
				'release-date' => '2012-10-29T12:54:41+00:00'
			);
		}

		public function getSource(){
			return '1';
		}

		public function allowEditorToParse(){
			return true;
		}

	}
