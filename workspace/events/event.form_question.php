<?php

	require_once(TOOLKIT . '/class.event.php');

	Class eventform_question extends SectionEvent{

		public $ROOTELEMENT = 'form-question';

		public $eParamFILTERS = array(
			'xss-fail'
		);

		public static function about(){
			return array(
				'name' => 'Form: Question',
				'author' => array(
					'name' => 'B Y',
					'website' => 'http://emigrant.by.localhost',
					'email' => 'by@klen.by'),
				'version' => 'Symphony 2.3',
				'release-date' => '2012-10-29T07:11:03+00:00',
				'trigger-condition' => 'action[form-question]'
			);
		}

		public static function getSource(){
			return '1';
		}

		public static function allowEditorToParse(){
			return true;
		}

		public static function documentation(){
			return '
        <h3>Success and Failure XML Examples</h3>
        <p>When saved successfully, the following XML will be returned:</p>
        <pre class="XML"><code>&lt;form-question result="success" type="create | edit">
  &lt;message>Entry [created | edited] successfully.&lt;/message>
&lt;/form-question></code></pre>
        <p>When an error occurs during saving, due to either missing or invalid fields, the following XML will be returned:</p>
        <pre class="XML"><code>&lt;form-question result="error">
  &lt;message>Entry encountered errors when saving.&lt;/message>
  &lt;field-name type="invalid | missing" />
  ...
&lt;/form-question></code></pre>
        <p>The following is an example of what is returned if any options return an error:</p>
        <pre class="XML"><code>&lt;form-question result="error">
  &lt;message>Entry encountered errors when saving.&lt;/message>
  &lt;filter name="admin-only" status="failed" />
  &lt;filter name="send-email" status="failed">Recipient not found&lt;/filter>
  ...
&lt;/form-question></code></pre>
        <h3>Example Front-end Form Markup</h3>
        <p>This is an example of the form markup you can use on your frontend:</p>
        <pre class="XML"><code>&lt;form method="post" action="" enctype="multipart/form-data">
  &lt;input name="MAX_FILE_SIZE" type="hidden" value="5242880" />
  &lt;label>Вопрос
    &lt;input name="fields[question]" type="text" />
  &lt;/label>
  &lt;label>Ссылка
    &lt;input name="fields[link]" type="text" />
  &lt;/label>
  &lt;label>Ответ
    &lt;textarea name="fields[answer]" rows="15" cols="50">&lt;/textarea>
  &lt;/label>
  &lt;label>К каким темам относится вопрос
    &lt;select name="fields[section][]" multiple="multiple">
      &lt;option value="docs">docs&lt;/option>
      &lt;option value="emigrant-100">emigrant-100&lt;/option>
      &lt;option value="visas">visas&lt;/option>
    &lt;/select>
  &lt;/label>
  &lt;label>Опубликовать
    &lt;input name="fields[publish]" type="checkbox" value="yes" />
  &lt;/label>
  &lt;label>Вывести на главную
    &lt;input name="fields[important]" type="checkbox" value="yes" />
  &lt;/label>
  &lt;label>Порядок сортировки
    &lt;input name="fields[order]" type="text" />
  &lt;/label>
  &lt;input name="action[form-question]" type="submit" value="Submit" />
&lt;/form></code></pre>
        <p>To edit an existing entry, include the entry ID value of the entry in the form. This is best as a hidden field like so:</p>
        <pre class="XML"><code>&lt;input name="id" type="hidden" value="23" /></code></pre>
        <p>To redirect to a different location upon a successful save, include the redirect location in the form. This is best as a hidden field like so, where the value is the URL to redirect to:</p>
        <pre class="XML"><code>&lt;input name="redirect" type="hidden" value="http://emigrant.by.localhost/success/" /></code></pre>';
		}

		public function load(){
			if(isset($_POST['action']['form-question'])) return $this->__trigger();
		}

	}
