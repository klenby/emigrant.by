$(document).ready(function() { 

    $('#form-faq').ajaxForm({ 
        dataType:  'xml', 
		beforeSubmit: validate,
        success:   processXml 
    });
	
	$("#form-faq").validate({
		validClass: "success",
		clearForm: true,
		
		highlight: function( element, errorClass, validClass ){
			$( element ).parent().addClass( errorClass ).removeClass( validClass );
		},
		
		unhighlight: function( element, errorClass, validClass ){
			$( element ).parent().addClass( validClass ).removeClass( errorClass );
		},
		
		showErrors: function( errorMap, errorList ){
			/* $("#summary").html("Your form contains " + this.numberOfInvalids() + " errors, see details below."); */
			if( this.numberOfInvalids() ){
				$( '#form-faq .submit' ).addClass( 'disabled' );
			} else {
				$( '#form-faq .submit' ).removeClass( 'disabled' );
			}
			this.defaultShowErrors();
		},
		
		messages: {
			'fields[email]' : {
				required : "Введите адрес электронной почты",
				email : "Адрес должен быть в виде name@domain.com"
			},
			'fields[question]' : {
				required : "Напишите свой вопрос"
			}
		}
	});
});

function processXml(responseXML) { 
    var message = $('message', responseXML).text();
	var result = $('result', responseXML).text();
    $('#form-result').html( '<div class="alert alert-' + result + '"><button type="button" class="close" data-dismiss="alert">×</button>' + message + '</div>' ); 
}

function validate(formData, jqForm, options){
	return $("#form-faq").valid();
}